#!/usr/bin/env python3
# Interpret Summit Service Log files ( a series of lines containing JSON Summit records )

# Open a log file and read all lines in it and present a standard summit server as a result

import sys
from protocol import summit
import json

def usage():
    print("Usage Message")
    print(""" Serial Summit protocol decoder will provide the conversation
    with summit systems timers over a direct serial link or 900MHz radio
    connected by serial or USBSerial.
    
    Usage ( including defaults )
    serve_log_files 
        --help provides this message
        --inputfilename=inputJSONFile.txt (for example)
        --overwrite
            Last record will be kept rather than first record

    """)

# Sample Command:
#     python3 serve_log_files.py --inputfilename=C:\Users\angus\Nextcloud\RaceFlow\summitlog_2024-02-18.txt --debug --api="localhost:8888" --api_format=html

########################################################################
def main():
    kwargs=dict()
    args=list()
    for a in sys.argv[1:]:
        if '=' in a:
            k,v=a.strip('-').split('=')
            kwargs[k]=v
        else :
            args.append(a)
    
    if '--help' in args:
        usage()
        exit (0)
        
    if 'inputfilename' in kwargs.keys():
        inputfilename = kwargs['inputfilename']
    else :
        print("--inputfilename= is a required parameter\n" )
        usage()
        exit (1)

    if '--overwrite' in args:
        overwrite = True
    else:
        overwrite = False

    if 'no_serial_reader' not in kwargs.keys() and '--no_serial_reader' not in args: kwargs['--no_serial_reader'] = 'True'
    args.append('--quiet')

    with summit(*args,**kwargs) as s: 
        # Read and process the log file
        with open(inputfilename,'r') as f:
            for r in f.readlines():
                if overwrite:
                    r = r.replace('}',', "overwrite":"True"}')
                r_value = s.add_JSON_data(r)
                if r_value is not None:
                    print (r_value, file=sys.stderr, flush=True)
        
        s.run()

        


if __name__ == '__main__':
    main()
