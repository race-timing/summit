Summit Timer Serial Protocol Reader
===================================

Serial Summit protocol decoder will provide the conversation
with summit systems timers over a direct serial link or 900MHz radio
connected by serial or USBSerial. It will provide records at the end
of the program being run, unless it is set up in either debug mode or 
in MQTT mode.

The MQTT mode is intended to provide the data directly on stdout for
consumption by mosquitto_pub, but may be used in other ways too.

The default mode of operation is to act on /dev/ttyUSB0, 9600 baud, 
and produce data in tab-delimited format file on stdout when the program ends.

Usage ( including defaults )
----------------------------

python3 protocol.py

        --debug
            Provide packet-level debug information as the process unfolds
            Value Options: '1','True','y','Y','true','TRUE' or no value are true
                --debug means True
                --debug=True means True
                --debug=0 means false
                --debug=Banana means false

        --devices=1,2,3,4,5,6,7,8,9
            A list of devices to poll in round-robin fashion. Must be a 
                comma-separated list of integers

        --port="/dev/ttyUSB0"
            required to be a valid value for the operating system
            Valid Options in Linux: /dev/ttyUSB0, /dev/serial0, /dev/AMA1, etc
            Valid Options in Windows: Com1, Com2, Com3, Com4, ...ComN

        --output
            Output filename, with extension. 
            If file ends with .csv, then output will be in .CSV format. 
            Defaults to tab-delimited text format.
            Line endings will be OS-dependant.

        --mqtt
            Provide direct output during operation, providing JSON on stdout.
            Should flush stdout

        --sync
            Send a sync signal over the serial port, which will properly 
            sync summits in range, but is not sync'd with computer clock
            time. There is a sync latency of between 0.25s and 0.30s to 
            computer clock time. There is no solution other than manual
            sync to a common GPS signal, or by calculating a correction
            factor after sync. This does not matter except to coordinate
            with devices sync'd to other clocks (like GPS). Consult the 
            summit timer documentation for procedures to perform a manual
            sync. Recommendations include using a GPS PPS signal gated to
            be once/minute.
    
Please note that this protocol may also be extended by providing a
live_output_callback function in a subclass. Import both summit and 
main for normal behaviour.

Sync over the serial port is caused by either putting the --sync flag
on the command line (for startup sync) or by sending signal SIGUSR1.

Operation as a systemd service
==============================
```
mkdir ~/.config
mkdir ~/.config/systemd
mkdir ~/.config/systemd/user

cp simulate_summits.service ~/.config/systemd/user/simulate_summits.service
systemctl --user daemon-reload
systemctl --user enable simulate_summits.service
systemctl status simulate_summits.service
```

Running on a Pi Zero as a USB Serial Device
===========================================
Add the USB Serial Bits to config.txt and cmdline.txt
- Add "dtoverlay=dwc2" to /boot/config.txt
- Add "modules-load=dwc2,g_serial" after "rootwait" in /boot/cmdline.txt
- Reboot

The USB serial device appears as /dev/ttyGS0 on the pi.

Connecting the Pi to a Windows computer presents a new serial port.


