#!/usr/bin/env python3 
####
# Reads from a set of summit timers to write out CSV files...
# Only parameters are port, outputfile, and device list
import sys
from protocol import summit


def usage():
    print("Usage Message")
    print(""" Serial Summit protocol decoder will provide the conversation
    with summit systems timers over a direct serial link or 900MHz radio
    connected by serial or USBSerial.
        
    The default mode of operation is to act on /dev/ttyUSB0, 9600 baud, 
    and produce data in CSV format files per summit.
    
    Usage ( including defaults )
    dump_summits
        --help provides this message
        --devices=1,2,3,4
            A list of devices to poll in round-robin fashion. Must be a 
                comma-separated list of integers
        --port="/dev/ttyUSB0"  or --port="COM8"
            Must be a valid value for the operating system
            Valid Options in Linux: /dev/ttyUSB0, /dev/serial0, /dev/AMA1, etc
            Valid Options in Windows: Com1, Com2, Com3, Com4, ...ComN
            If not specified, will search for serial ports
        --output
            Output filename, with extension. 
			CSV or Tab-delimited text files.
			By default, the files will be "Summit_{device}.CSV" with the {device} replaced by 
				the summit number.

    """)



########################################################################
def main():
    kwargs=dict()
    args=list()
    for a in sys.argv[1:]:
        if '=' in a:
            k,v=a.strip('-').split('=')
            kwargs[k]=v
        else :
            args.append(a)
    if 'output' not in kwargs.keys(): outputpattern="Summit_{device}.CSV"
    else: outputpattern=kwargs['output']
    if 'device' in kwargs.keys():
        kwargs['devices']=kwargs['device']
    if 'devices' not in kwargs.keys():
        kwargs['devices']='1'
    if '--noapi' not in args: args.append('--noapi')
    kwargs['download_only']=2
	
    if '--help' in args:
        usage()
        exit (0)

    for device in kwargs['devices'].split(','):
        kwargs['output']=outputpattern.format(device=device)
        kwargs['devices']=device
        print ("Downloading from summit {devices}, to file {output}".format(**kwargs))
        with summit(*args,**kwargs) as s: 
            s.run()
            s.quit()

        break
        


if __name__ == '__main__':
    main()
