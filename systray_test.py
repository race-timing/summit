#This program need appindicator support, and seems to need gnome-shell-extensions-top-icons-plus
# but it may also need some other detail. Install gnome-tweaks too...

# Hmmm... Don't know if it still works in windows.
# Yup... Works in Windows 7 OSGeo4w...

# Taken from https://stackoverflow.com/questions/6389580/quick-and-easy-trayicon-with-python

import wx

TRAY_TOOLTIP = 'System Tray Demo'
TRAY_ICON = 'icon.png'
TRAY_ICON_RED = 'icon_red.png'

def create_menu_item(menu, label, func):
	item = wx.MenuItem(menu, -1, label)
	menu.Bind(wx.EVT_MENU, func, id=item.GetId())
	menu.AppendItem(item)
	return item

class TaskBarIcon(wx.TaskBarIcon):
	def __init__(self, frame):
		self.icon_choice = 'green'
		self.frame = frame
		super(TaskBarIcon, self).__init__()
		self.set_icon(TRAY_ICON)
		self.Bind(wx.EVT_TASKBAR_LEFT_DOWN, self.on_left_down)

	def CreatePopupMenu(self):
		menu = wx.Menu()
		create_menu_item(menu, 'Say Hello', self.on_hello)
		menu.AppendSeparator()
		create_menu_item(menu, 'Exit', self.on_exit)
		return menu

	def set_icon(self, path=None):
		if path is None:
			if self.icon_choice == 'green':
				self.icon_choice = 'red'
				path=TRAY_ICON_RED
			else:
				self.icon_choice = 'green'
				path=TRAY_ICON

		icon = wx.IconFromBitmap(wx.Bitmap(path))
		self.SetIcon(icon, TRAY_TOOLTIP)

	def on_left_down(self, event):
		self.set_icon()
		print 'Tray icon was left-clicked.'

	def on_hello(self, event):
		print 'Hello, world!'

	def on_exit(self, event):
		wx.CallAfter(self.Destroy)
		self.frame.Close()

class App(wx.App):
	def OnInit(self):
		frame=wx.Frame(None)
		self.SetTopWindow(frame)
		TaskBarIcon(frame)
		print 'Init The App'
		return True

def main():
	app = App(False)
	app.MainLoop()


if __name__ == '__main__':
	main()

