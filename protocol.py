#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
#
#  protocol.py
#
#  Copyright 2018-2024 Angus Carr <angus.carr@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

# This file contains the protocol for talking to the Summit SRT-1000
#
# Implementation ideas ought to follow the Summit Timing DLL for naming,
# if only for ease of understanding and communicating.
# In general, these threads are in use:
#   reader: intake and intepretation of incoming data
#   writer: conducts the summit conversation with multiple units (could be main thread)
#   webserver: provides a simple api for requesting data

import serial
import time
import datetime
import json

import threading

import signal, os, sys

# Unbuffered output on print for everything...:
#import functools
#print = functools.partial(print, flush=True)

from functools import wraps
def named_function(f):
    @wraps(f)
    def wrapper(*args,**kwargs):
        print('Calling {}: \n\targs({})\n\tkwargs({})'.format(f.__name__, args, kwargs))
        return f(*args,**kwargs)
    return wrapper

from crc import summit_crc
CRLF = '\r\n'
global_packets=dict()

class summit( ):
    serial_reader = True
    ser = None
    devices = [ ]
    device_AK = { } # time of most recent packet
    device_record = { }
    transmit_alive = False
    alive = False
    outputfilename = None
    outputfile = None
    mqtt = True
    sep = '\t'
    sync_on_start = False
    debug=False
    port=None
    quiet=False
    simulate_summits=False
    summit_token=None
    summit_token_reset=False
    verbose=False
    live_output_callback = None
    api=True
    api_port=8080
    api_format='json'
    hostname = 'localhost'
    dummy=False
    download_only=False
    download_only_count=1

    thread_read=None
    thread_transmit=None
    thread_api=None

    #@named_function
    def __init__(self,*args,**kwargs ):
        self.alive=False
        self.transmit_alive = False

        # Check for debug
        if 'debug' in kwargs.keys() :
            if  kwargs['debug'] in [ '1','True','y','Y','true','TRUE' ]:
                self.debug=True
        if '--debug' in args:
            self.debug=True
        if self.debug: print("Operating in DEBUG mode")



        if 'devices' in kwargs.keys():
            self.devices = [ int(d) for d in kwargs['devices'].split(',') ]
        elif 'device' in kwargs.keys(): #Technically a misspelling... but anyway...
            self.devices = [ int(d) for d in kwargs['device'].split(',') ]
        else:
            self.devices = [ int(d) for d in [1,2,3,4,5,6,7,8,9] ]

        if self.debug: print("Operating on devices: {}".format(self.devices))

        # Check for port parameter
        if 'port' in kwargs.keys():
            self.port = kwargs['port']
        if self.debug: print("On port: {}".format(  str(self.port) ))

        try:
            signal.signal(signal.SIGUSR1, self.transmit_sync_now )
        except AttributeError as e:
            pass

#        try:
#            signal.signal(signal.SIGUSR2, self.drop_records )
#        except AttributeError as e:
#            pass

        # output file?
        if 'output' in kwargs.keys():
            self.outputfilename = kwargs['output']
        try:
            self.outputfilename = [i for i in args if i[-3:].lower() in ['txt', 'csv']][0]
            if self.outputfilename[-3:].lower() == 'csv':
                self.sep=','
            elif self.outputfilename[-3:].lower() == 'txt':
                self.sep='\t'  # already the default... :-)
        except AttributeError:
            pass
        except IndexError:
            pass

        self.sync_on_start = ( '--sync' in args )

        if self.debug and not self.outputfilename : print(self.sep.join(self.packet_fieldnames))

        if 'nomqtt' in kwargs.keys() or '--nomqtt' in args:
            self.mqtt=False
        if 'mqtt' in kwargs.keys() or '--mqtt' in args:
            self.mqtt=True

        if 'quiet' in kwargs.keys() or '--quiet' in args:
            self.quiet=True
        if '--simulate_summits' in args:
            self.quiet=True
            self.simulate_summits=True
            #print("Simulating Summits... Woohoo!!!")
        if 'verbose' in kwargs.keys() or '--verbose' in args:
            self.verbose=True
        if 'noapi' in kwargs.keys() or '--noapi' in args:
            self.api=False
        if '--dummy' in args:
            self.dummy=True
        if 'api' in kwargs.keys():
            try: #if ':' in kwargs['api']:
                self.api_port = int(kwargs['api'].split(':')[1])
                self.hostname = kwargs['api'].split(':')[0]
		# Check if blank hostname, then use IP number
                if self.hostname == '':
                     import socket
                     self.hostname=socket.gethostbyaddr(socket.gethostname())[2][0]
            except IndexError:
                self.hostname = 'localhost'
                self.api_port=int(kwargs['api'])
        if 'api_format' in kwargs.keys():    self.api_format=kwargs['api_format']

        if '--download_only' in args:
            self.download_only=True
            if self.debug: print("Download only mode: {}".format(self.download_only_count))
        if 'download_only' in kwargs.keys():
            self.download_only=True
            self.download_only_count=int(kwargs['download_only'])
            if self.debug: print("Download only mode: {}".format(self.download_only_count))

        if '--no_serial_reader' in args:
            self.serial_reader = False
        elif 'no_serial_reader' in kwargs.keys():
            self.serial_reader = upper(kwargs['no_serial_reader']) in ['TRUE','1','Y']

    def find_port(self):
        """
        Looks for a port on the principle that a serial USB device on USB contains the string "Digi"
        """
        from serial.tools import list_ports
        #known_serial_modem_hwid = [ 'USB VID:PID=0403:6001', # Digi XBEE RF Modem (USB) - FTDI chip          
        #    'USB VID:PID=067B:2303'                          # Black USB Serial Cable - Prolific chip, also older USB Serial devices.
        #    ]
        # USB VID:PID=0557:2008 LOCATION=1-1.4 # Doesn't work in Windows - too old...
        # (0x0525,0xA4A7) is a Pi Zero USB Serial device
        known_serial_modem_hwid = [(0x067B,0x2303),(0x0403,0x6001),(0x0557,0x2008),(0x0525,0xA4A7)]
        for p in list_ports.comports():
            p_hwid=(p.vid,p.pid) #p.hwid.split("SER")[0].strip() # Get rid of serial number...
            if self.debug: print ("Port search testing {device} - {manufacturer} - {hwid} - {vid}:{pid}".format(**(p.__dict__)))
            if  p_hwid in known_serial_modem_hwid : # p.manufacturer in [ "Prolific","FTDI" ]:
                if p.description == 'PL2303HXA PHASED OUT SINCE 2012. PLEASE CONTACT YOUR SUPPLIER.':
                    print ("\tBlacklisting Old USB Serial version\n")
                    continue
                if self.debug: print ("\tFound based on {hwid}".format(**(p.__dict__)))
                return p.device
            else :
                if self.debug: print ("\tSerial Device Ignored based on {hwid}".format(**(p.__dict__)))



    def open(self,port=None):
        if port is None:
            port=self.find_port()
        try:
            self.ser = serial.Serial(port, 9600, timeout=1, rtscts=0 )
        except serial.serialutil.SerialException as e:
            if self.debug: print ("Cannot find a serial port. Continuing for development.")
        if self.debug: print (f"Using serial port {self.ser.name}")
        self.alive = True

########################################################################
# And now we have the protocol interpreter                             #
########################################################################
    packet_fieldnames = 'device,record,event,heat,channel,type,label,time'.split(',')


    def interpret_packet( self,  packet_string ):
        """
SRT 1000 Communications Protocol:
    Data Packet is tab delimited)

{ Device (tab) Record (tab) Event (tab) Heat (tab) Channel (tab)  Type  (tab) User String (tab) Time         } CRC  (CR-LF)
{    1   (tab)   57   (tab)  20   (tab)   5  (tab)    1    (tab)    b   (tab) 45          (tab) hh:mm:ss.ttt } abcd (CR-LF)

Record type is:

"b" for bib (ie user string)
"s" for switch closure (ie impulse)
"y" for synch inpulse
"p" for posting bib (ie time with posting bib)

Capital letters indicate that the F4 key was used instead of plunger.  For instance,
"S" for switch closure from F4 key
"P" for posting bib from F4 key
        """
        if self.debug: print("Interpreting packet :{0}".format(packet_string))
        if '}' not in packet_string:
            if self.debug: print ("Found no brace close")
            return None
        try:
            p,c = packet_string.strip('\r\n').split('}')
        except ValueError as e:
            #  FIXME: Appears as multiple records in listen mode, I think
            #  My suspicion is that the active server is making a request as
            #  the summit is still responding to the previous request.
            #  Treating it as a corrupt packet will mean that hopefully it will
            #  be re-transmitted by the summit, and will get picked up that way.
            print(e,file=sys.stderr,flush=True)
            print(packet_string,file=sys.stderr,flush=True)
            return None

        verbosity = 0
        if self.verbose: verbosity = 1

        try:
            v = int(c,16)
        except ValueError as e:
            if self.debug: print(str(e))
            return None

        safeasinteger=lambda x: None if str(x) == "" else ( int(x) if x.isdigit() else x )

        if int(c, 16) != summit_crc( p[1:] ):
            if self.debug: 
                print("checksum failure")
                print("checksum should be {}".format(hex(summit_crc( p[1:] ))))
        else:
            # Check for status packets
            if p[1:3] == 'TX': #TX 2: Low Auxiliary Battery
                result = {'device': safeasinteger(p[3:].split(":")[0].strip()) ,  'Warning':p.split(':')[1].strip() }
            elif p[1:3]=='AK': # No new information packet
                dev=safeasinteger(p[3:].strip())
                result = {'device': dev}
#                if dev not in global_packets.keys():
#                    global_packets[dev] = dict()
                if dev in self.device_AK.keys(): self.device_AK[ dev ] += 1
                else: self.device_AK[dev]=1
            elif p[1:3]=='TK': # Listening to another sender (Quiet mode)
                #"{TK {d} {r}"
                d = safeasinteger(p[3:].strip().split(" ")[0])
                if d==0:
                    #Be Quiet Command
                    result={'TalkCommand':d}
                else:
                    r = None
                    try:
                        r = safeasinteger(p[3:].strip().split(" ")[1])
                    except IndexError:
                        pass
                    result = {'TalkCommand':d , 'record': r }
                self.summit_token=result
                self.summit_token_reset=True
            elif p[1:3]=='SY': # Time ACK packet
                #print(p[4:])
                dev = safeasinteger(p[4:].split(':')[0].strip(':'))
                t = ''.join(p[4:].split(':')[-3:])
                result =  {'device': dev,'time':t.strip()}
                #print (result)
            else :
                result =  dict(zip( self.packet_fieldnames, [ safeasinteger(i) for i in p[1:].split('\t') ] ))

                dev = result['device']
                rec = result['record']
                self.device_record[dev]=rec

                # Store received packets by device
                if dev not in global_packets.keys():
                    global_packets[dev] = dict()
                global_packets[dev][rec] = result

                if self.debug: self.print_record([ result[k] for k in self.packet_fieldnames if k in result.keys() ])

                verbosity = 1

                #Reset counter of AK's
                self.device_AK[ dev ] = 0

            # Dump records out, including AKs and other protocol traffic
            if self.mqtt and verbosity :
                self.print_record(json.dumps(result),flush=True)

            # if there's a call back, call back...
            if self.live_output_callback :
                self.live_output_callback( result )

            return result
        return None

    def status_message(self,msg,label='status'):
        if self.mqtt:
            print('{'+'"{}":"{}"'.format(label,msg)+'}')
        else:
            print (str(msg))

    ########################################################################
    # Read from timer to PC                                                #
    ########################################################################
    def reader(self):
        """
        Loop forever and read packets
        """
        endOfRead = True
        msg= ''

        data=None # data buffer name allocation

        self.status_message('reader thread started')

        if self.dummy: self.add_dummy_data()
        while self.alive:
            #data = self.ser.read(self.ser.in_waiting or 1)

            try:
                data = self.ser.read(max(self.ser.in_waiting , 1))
            except AttributeError:
                time.sleep(1)
            except serial.serialutil.SerialException:
                time.sleep(1)
            #print(data)
            if data:
                msg+= data.decode('UTF-8')
                if '\n' in msg:
                    msgs=msg.split('\n')
                    msg=msgs[-1]
                    if self.debug: print(msgs[0].strip('\n'))
                    for i in msgs[0:-1]: self.interpret_packet(i)

            # Check if we should quit from exhaustion
            if self.download_only and self.device_AK.values() :
                #print("AK_values: {} > {}?".format(self.device_AK.values(),self.download_only_count))
                if min ( self.device_AK.values() ) > self.download_only_count :
                    self.alive = False

        self.alive = False
        self.status_message('reader thread terminated')


    #FIXME - implementation should change to use the threaded pyserial
    # ref: https://pyserial.readthedocs.io/en/latest/pyserial_api.html#module-serial.threaded
    # should let us launch a thread, then call self.interpret_packet on any rec'd lines - thread safe port handling.

    def launch_reader_thread(self):
        """
        Connect to the serial port and read packets and handle them...
        """
        self.alive = True
        self.thread_read = threading.Thread(target=self.reader)
        self.thread_read.daemon = True
        self.thread_read.name = 'summit_reader'
        self.thread_read.start()

########################################################################
# Send from PC to timer                                                #
########################################################################
    def transmit_packet(self, packet,override_quiet=False):
        """
        Mechanical process of wrapping the packet in curly braces and CRC.
        The packet is passed exactle as provided.
        Program parameter --quiet removes all writes caused by the 
            polling system.
        Optional parameter override_quiet is to allow writes as a client (summit simulator)
        """
        if override_quiet or not self.quiet :
            p_crc = summit_crc(packet)
            full_packet = bytes('{o}{packet}{c}{p_crc:04x}\r\n'.format(packet=packet,p_crc=p_crc,o='{',c='}'),'UTF-8')

            if self.debug: print ("sending %s"%(full_packet))

            try:
                self.ser.write(full_packet)
            except AttributeError:
                # port not open... Lose the data
                pass
#            except serial.serialutil.PortNotOpenError:
                #Try again after opening the port... :-)
            except serial.serialutil.SerialException:
                self.open()
                self.ser.write(full_packet)



    hex = lambda self,f: ':'.join(hex(x)[2:] for x in f )

    def transmit_beQuiet(self):
        """
        Tell all devices to stop talking. 
        Should wait for 50 milliseconds to allow packet being transmitted to finish.
        """
        self.transmit_packet('TK 0')

    def transmit_giveToken(self,device):
        """
        Give the token to device. Device will give back records from 
        whatever they last sent, or respond with an AK.
        This function is equivalent to transmit_query(device).
        """
        self.transmit_packet('TK {d}'.format(d=str(device)))

    def transmit_query(self,device,record=None):
        """So device #1 will respond with any data, starting at record #1.
            Wait 200 milliseconds for data.
            Suppose that device #1 tramits 10 records that pass
            error checking.
            If Device #1 has no data, it will respond with 'AK 1'
        """
        if record is None:
            self.transmit_packet('TK {d}'.format(d=str(device)))
        else:
            self.transmit_packet('TK {d} {r}'.format(d=str(device),r=str(record)))

    def transmit_reset(self):
        """Reset Packet (note that the space is required)"""
        #{RS } CRC CR LF
        self.transmit_packet('RS ')

    def transmit_disableReset(self):
        """Disable Reset (so timer ignores further reset commands): {RS x} CRC CR LF """
        self.transmit_packet('RS x')

    def transmit_sync(self,h,m,s):
        """Synch Packet: {SY 0:0:0.0} CRC CR LF   """
        self.transmit_packet('SY {h}:{m}:{s}'.format(h=h,m=m,s=float(s)))

    def transmit_sync_now(self,*args,**kwargs):
        """Sync to computer time with minimum latency"""
        self.transmit_packet(datetime.datetime.now().strftime("SY %H:%M:%S.%f") )

    def transmit_syncAdjust(self,h,m,s):
        """Synch Offset (adjusts timer's synch time by the amount specified): {SYO 0:0:0.0} CRC CR LF """
        self.transmit_packet('SY0 {h}:{m}:{s}'.format(h=h,m=m,s=s))

    def transmit_setEventHeat(self, device, event, heat):
        """Set Event and Heat: {EV 0 26 20} CRC CR LF"""
        self.transmit_packet('EV {d} {e} {h}'.format(d=device,e=event, h=heat))

    ################
    # Transmit Thread
    def launch_writer_thread(self):
        """
        Connect to the serial port and write request packets
        """
        self.transmit_alive = True
        if self.simulate_summits:
            self.thread_transmit = threading.Thread(target=self.simulator_summits)
            #print ("Launched simulator_summits trhead")
        else:
            self.thread_transmit = threading.Thread(target=self.transmitter)
            #print ("Launched Polling Transmitter thread")
        self.thread_transmit.daemon = True
        self.thread_transmit.name = 'summit_transmitter'
        self.thread_transmit.start()

    #################
    # Simulating a summit transmitter
    def simulator_summits(self):
        """
        Simulate summits as specified on the command line
        """
        self.transmit_alive = True
        self.status_message('Simulated Summit started')
        self.summit_token = {'TalkCommand':0} # Initial state - assume quiet

        record_pattern=""
        for d in self.packet_fieldnames:
            record_pattern += '{{{}}}\t'.format(d)
        record_pattern = record_pattern.strip('\t')
        print(record_pattern)

        while self.transmit_alive and self.alive :
            if not self.summit_token_reset :
                time.sleep(0.1)
                continue
                #if self.debug: print ("\ttokenFalse")
            #{'TalkCommand':d,"record":r}
            dev = self.summit_token["TalkCommand"]
            if dev == 0 or dev not in self.devices:
                # Be Quiet Command
                #if self.debug: print ("\ttokenTrue_Quiet( {} )".format(self.summit_token))
                time.sleep(0.1)
                continue
            if dev in self.devices :
                # Respond with appropriate record
                self.summit_token_reset=False
                r = self.summit_token["record"]
                # Means next record?
                if r is None and dev not in self.device_record.keys(): r = 1
                # r is the record after the last transmitted...
                elif r is None: r = self.device_record[dev] + 1

                if dev not in global_packets.keys():
                    #if self.debug: print ("\ttokenTrue_AK( {} )".format(dev))
                    self.transmit_packet("AK {}".format(dev),override_quiet=True)
                    continue

                if r not in global_packets[dev].keys():
                    #if self.debug: print ("\ttokenTrue_AK( {} )".format(dev))
                    self.transmit_packet("AK {}".format(dev),override_quiet=True)
                    continue

                #Hammer through records until it's time to stop, or we run out.
                while not self.summit_token_reset :
                    try:
                        record = global_packets[dev][r]
                    except KeyError:
                        break
                    response=record_pattern.format(**record)

                    if self.debug: print ("\tRecord Transmit( {} )".format(response))
                    self.transmit_packet(response,override_quiet=True)
                    self.device_record[dev]=r
                    r=r+1


    #################
    # transmit thread function
    def transmitter(self):
        """
        generic transmit function, for use in main thread or sub-thread.
        Performs round-robin polling through known devices
        """
        self.transmit_alive = True
        while self.transmit_alive and self.alive :
            for dev in self.devices :
                self.transmit_beQuiet()
                time.sleep(0.1)

                # Devices enumerated have been entered into global_packets 

                if dev not in global_packets.keys():
                    # First packet to an unknown device...
                    #print("{} not found yet".format(dev))
                    self.transmit_query(device=dev, record=1)
                else:
                    # if device has never rec'd a packet
                    if global_packets[dev] is None :
                        #print("Looking for first record from {}".format(dev))
                        self.transmit_query(device=dev, record=1)
                    # test for discontinuous packet list
                    elif len(global_packets[dev]) != max ( global_packets[dev].keys()  )  :
                        ### This should test for the list not being complete
                        #print ("missing record - requesting retransmit ({} != {}".format(len(global_packets[dev]),max ( global_packets[dev].keys() )))
                        for i in range(1,max ( global_packets[dev].keys()) ) :
                            if ( i not in global_packets[dev].keys() ):
                                self.transmit_query(device=dev,record=i)
                                break
                    else :
                        self.transmit_giveToken(dev)
                time.sleep(0.5)
            time.sleep(0.1)

    @named_function
    def drop_records(self,*args,**kwargs):
        """
        Drop all summit records, to set up a clean dataset
        Intended for use as a SIGUSR2 handler
        """
        global_packets=dict()


    def quit(self):
        """
        Quit gracefully, waiting for the read thread to exit
        """
        self.alive = False
        self.transmit_alive = False
        #if self.ser: 
        if self.thread_read: self.thread_read.join()
        #if self.thread_api: self.thread_api.join()
        #if self.thread_transmit: self.thread_transmit.join()
        try:
            self.output_records()
        except ValueError:
            pass
        
        try:
            self.ser.close()
        except AttributeError:
            # port is None 
            pass
        self.packets=None
        self.device_AK = None
        self.device_record = None
        global_packets = None

    def print_record(self,record_text,**kwargs):
        #This fails in python 2 - hmmmm.
        if 'flush' in kwargs.keys() :
            print( record_text, flush=kwargs['flush'] )
        else:
            print( record_text,flush=True )

    def output_record(self,r):
        if self.outputfile is None and not self.outputfilename :
            self.print_record(self.sep.join([ str(r[k]) for k in self.packet_fieldnames ]))
            return True
        if self.outputfile is None: 
            self.outputfile=open(self.outputfilename,'w')
        self.outputfile.write(self.sep.join([ str(r[k]) for k in self.packet_fieldnames ]))
        self.outputfile.write('\n')

    def output_records(self):
        self.status_message("Writing Records on quit")
        if ( self.mqtt and not self.verbose and not self.outputfilename ) : return None
        
        #header...
        
        self.output_record( dict(zip(self.packet_fieldnames,self.packet_fieldnames)) )
        for d,v in global_packets.items():
            for r,p in v.items():
                self.output_record( p )
        if self.outputfilename:
            self.outputfile.close()

    def __enter__(self):
        self.open(self.port)
        return self
        
    def __exit__(self,*args,**kwargs):
        self.quit()
        

    def run (self):
        # defult run function after creation
        # Launch Reader Thread...
        if self.serial_reader: self.launch_reader_thread()
        if self.api: self.launch_web_thread()
    
        if self.sync_on_start: self.transmit_sync_now()

        try:
            if self.simulate_summits:
                #print ("simulator_summits in main thread")
                self.simulator_summits()
            else:    
                #print ("Polling Transmitter in main thread")
                self.transmitter()
        except KeyboardInterrupt :
            self.quit()


    ###########################
    def launch_web_thread(self):
        """
        Launch a thread with a webserver which produces the dataset on request
        """
        self.thread_api = threading.Thread(target=self.webserver)
        self.thread_api.daemon = True
        self.thread_api.name = 'summit_api'
        self.thread_api.start()
        
    def webserver(self):
        """
        webserver which provides JSON data on request. 
        GET / will just get all the data.
        GET /{device} will get data for a device
        GET /{device}/{record} will get data for a particular record
        GET /{device}/{record}/{label} will get a plain text version of the record parameter.
        POST application/json to add a record
            "device","record","event","heat","channel","type","label","time"
        """
        # Python 3 server example
        from http.server import BaseHTTPRequestHandler, HTTPServer
        import time
        import cgi

        hostName = self.hostname
        global global_packets
        summit_labels  = self.packet_fieldnames

        if self.api_format == 'html':
            Content_type="text/html"
            header_pattern='<html><head><title>summit service</title></head>\n<body>\n<table id="summits"><tr><th>{}</th></tr>\n'.format('</th><th>'.join(self.packet_fieldnames))
            record_pattern='<tr><td>{'+ '}</td><td>{'.join(self.packet_fieldnames) + '}</td></tr>'
            footer_pattern="\n</table></body></html>"
            record_separator = '\n'
        else : #default to JSON
            Content_type="application/json"
            header_pattern='[\n'
            
            record_pattern='\t{{\n\t\t' 
            for d in self.packet_fieldnames:
                if d in self.packet_fieldnames[-3:]:
                    record_pattern += '"{}":"{{{}}}"'.format(d,d)
                else :
                    record_pattern += '"{}":{{{}}}'.format(d,d)
                if d != self.packet_fieldnames[-1]: record_pattern += ',\n\t\t'
            #'}",\n\t"{'.join(self.packet_fieldnames) + 
            record_pattern +='\n\t}}'
            
            footer_pattern='\n]'
            record_separator=',\n'

        if self.debug:
            print ("record_pattern:\t",record_pattern)


        class MyServer(BaseHTTPRequestHandler):
            def respond(self,response=200,content_type='text/plain',body=""):
                self.send_response(response) # Request succeeded, User should look elsewhere for information...
                self.send_header("Content-type", content_type)
                self.end_headers()
                self.wfile.write(bytes(body,"utf-8"))
                return  ( response >= 200 and response < 200 )

            def do_GET(self):
                #self.wfile.write(bytes("<p>Request: %s</p>" % self.path, "utf-8"))
                devices=sorted(list(global_packets.keys()))
                dev=None
                try:
                    dev=self.path.strip('/').split('/')[0]
                    if dev=="": pass
                    elif int(dev) in devices:
                        dev = int(dev)
                        devices = [ dev ]
                    else : return self.respond(response=303,body="No records for Device")
                except ValueError:
                    return self.respond(response=303,body="Device number not properly formed")
                rec = None
                try:
                    rec=int(self.path.strip('/').split('/')[1])
                    if rec > max(global_packets[dev].keys()):
                        # Request succeeded, but no content to provide...
                        return self.respond(response=204) #,body="Record number not yet present"
                    elif rec == 0:
                        # Request succeeded, User should look elsewhere for information...
                        return self.respond(response=303,body="Record number 0")
                except ValueError:
                        # Request succeeded, User should look elsewhere for information...
                        return self.respond(response=303,body="Record number not properly formed")
                except IndexError: 
                    pass

                label = summit_labels 
                try:
                    l = self.path.strip('/').split('/')[2]
                    if rec in global_packets[dev].keys() and l in summit_labels :
                        # if we get here, they are asking for the data to be written directly... No formatting
                        return self.respond(response=200,
                            body=str(global_packets[dev][rec][l])
                            )
                    else:
                        return self.respond(response=303,body="label not properly formed")
                except IndexError:
                    #Didn't have a term [2]...
                    pass

                self.send_response(200)
                self.send_header("Content-type", Content_type)
                self.end_headers()

                # Body of message
                self.wfile.write(bytes(header_pattern,"utf-8"))
                # Write all records in pattern
                #records = list()
                for d in devices:
                    last_device=(d==devices[-1])
                    for r in sorted(list(global_packets[d].keys())):
                        if rec is None:
                            last_record=(d==max(global_packets[d][r].keys()))
                        else:
                            # Specified a record.
                            last_record=True
                            if r != rec : continue                   
                        try:
                            record = global_packets[d][r]
                            if record['label'] is None: record['label'] = '' 
                            self.wfile.write(bytes( record_pattern.format(**record)  ,"utf-8" ))
                        except KeyError as e:
                            print (e)
                            print(record_pattern)
                            print(global_packets[d][r])
                            #exit (1)
                        except ValueError as e:
                            print (e)
                            print(record_pattern)
                            print(global_packets[d][r])
                            #exit (1)
                        if not (last_device and last_record): self.wfile.write(bytes(  record_separator ,"utf-8" ))
                self.wfile.write(bytes(  footer_pattern ,"utf-8" ))

            # POST echoes the message adding a JSON field
            def do_POST(self):
                ctype = dict(self.headers.__dict__['_headers'])['Content-Type']
                # refuse to receive non-json content
                if ctype != 'application/json':
                    return self.respond(response=400,body="POST type application/json")
                    
                # read the message and convert it into a python dictionary
                length = int(dict(self.headers.__dict__['_headers'])['Content-Length'])
                m=self.rfile.read(length).decode("utf-8")
                try:
                    message = json.loads( m )
                except json.decoder.JSONDecodeError:
                    return self.respond(response=400, body="JSON body improperly formed")

                # If message appears to be ok, then consume!
                # Required: "device","type"
                for n in ["device","type"]:
                    if n not in message.keys(): return self.respond(response=400, body="{} required parameter".format(n))
                dev=int(message['device'])

                # Fill out message parameters
                if 'time' not in message.keys():
                    t = time.time_ns()
                    message['time']="{0}.{1:06d}".format(time.strftime("%H:%M:%S",time.localtime(t/1000000000)),int((t/1000)%1000000))
                if 'event' not in message.keys(): message['event'] = 0
                if 'heat' not in message.keys(): message['heat'] = 0
                if 'channel' not in message.keys(): message['channel']=1
                if 'label' not in message.keys(): message['label']=''
                if 'record' not in message.keys() and dev in global_packets.keys():
                    message['record'] = max(global_packets[dev].keys()) + 1
                elif 'record' not in message.keys():
                    message['record'] = 1
                rec=int(message['record'])

                # Validate mesage requirements
                if message['type'] in ['b','B','p','P'] and message['label'] in [ None, ""]:
                    return self.respond(response=400, body="label is required parameter when type {type}".format(**message))


                if dev not in global_packets.keys():
                    global_packets[dev]={rec:message}
                elif rec not in global_packets[dev].keys():
                    global_packets[dev][rec]=message
                elif 'overwrite' in message.keys():
                    global_packets[dev][rec]=message
                               
                # send the message back
                return self.respond(200, body="OK")

# curl --header "Content-Type: application/json" --request POST --data {"device":9,"record":1,"event":0,"heat":0,"channel":1,"type":"p","label":null,"time":"01:02:03.321"} http://localhost:8080            

        webServer = HTTPServer((hostName, self.api_port), MyServer)
        self.status_message("WebServer started http://%s:%s" % (hostName, self.api_port))

        try:
            webServer.serve_forever()
        except KeyboardInterrupt:
            pass

        webServer.server_close()
        self.status_message("Web Server stopped.")
        
    def add_JSON_data(self,json_packet):
        """
        Recieves a record in JSON format and adds it into the stream
        """
        try:
            message = json.loads( json_packet )
        except json.decoder.JSONDecodeError:
            return (f"JSON body improperly formed {json_packet}")

        # If message appears to be ok, then consume!
        # Ignore status messages
        for n in ["status"]:
            if n in message.keys(): return None
        
        # Required: "device","type"
        for n in ["device","type"]:
            if n not in message.keys(): return (f"{n} required parameter {message}")
        dev=int(message['device'])

        # Fill out message parameters
        if 'time' not in message.keys():
            t = time.time_ns()
            message['time']="{0}.{1:06d}".format(time.strftime("%H:%M:%S",time.localtime(t/1000000000)),int((t/1000)%1000000))
        if 'event' not in message.keys(): message['event'] = 0
        if 'heat' not in message.keys(): message['heat'] = 0
        if 'channel' not in message.keys(): message['channel']=1
        if 'label' not in message.keys(): message['label']=''
        if 'record' not in message.keys() and dev in global_packets.keys():
            message['record'] = max(global_packets[dev].keys()) + 1
        elif 'record' not in message.keys():
            message['record'] = 1
        rec=int(message['record'])

        # Validate mesage requirements
        if message['type'] in ['b','B','p','P'] and message['label'] in [ None, ""]:
            return ("label is required parameter when type {type}".format(**message))

        if dev not in global_packets.keys():
            global_packets[dev]={rec:message}
        elif rec not in global_packets[dev].keys():
            global_packets[dev][rec]=message
        elif 'overwrite' in message.keys():
            global_packets[dev][rec]=message

    
    def add_dummy_data(self):
        self.interpret_packet("{TX 2: Low Auxiliary Battery}ad78")
        self.interpret_packet("{2\t1\t0\t0\t1\tb\t\t0:00:03.300}23fe")        # Enter Key
        self.interpret_packet("{2\t2\t0\t0\t1\tS\t\t0:00:07.894}563a")        # Impulse Key?
        self.interpret_packet("{2\t3\t0\t0\t1\tP\t123\t0:00:24.956}3eb9")    # post and plunge
        self.interpret_packet("{2\t4\t0\t0\t1\tP\t123\t0:02:37.925}6995")
        self.interpret_packet("{2\t5\t0\t0\t1\tb\t\t0:02:41.644}4f51")
        self.interpret_packet("{2\t6\t0\t0\t1\tb\t\t0:02:43.847}587a")
        self.interpret_packet("{AK 2}3ffc")
        self.interpret_packet("{2\t7\t0\t0\t1\tb\t123\t0:02:54.550}1b99")
        self.interpret_packet("{2\t8\t0\t0\t1\tb\t555\t0:02:58.331}5da0")
        
        
        
        
########################################################################
# End of Module      

def testing(s):
    print("testing some demo packets...")
    print (s.interpret_packet("{TX 2: Low Auxiliary Battery}ad78"))
    print (s.interpret_packet("{2   1   0   0   1   b        0:00:03.300}b152"))        # Enter Key
    print (s.interpret_packet("{2   2   0   0   1   S        0:00:07.894}d170"))        # Impulse Key?
    print (s.interpret_packet("{2   3   0   0   1   P   123  0:00:24.956}ff2a"))    # post and plunge
    print (s.interpret_packet("{2   4   0   0   1   P   123  0:02:37.925}b942") )
    print (s.interpret_packet("{2   5   0   0   1   b        0:02:41.644}dd8a") )
    print (s.interpret_packet("{2   6   0   0   1   b        0:02:43.847}3a86") )
    print (s.interpret_packet("{AK 2}3ffc") )
    print (s.interpret_packet("{2   7   0   0   1   b   123  0:02:54.550}18a1") )
    print (s.interpret_packet("{2   8   0   0   1   b   555  0:02:58.331}bff4") )
    return 0

def usage():
    print("Usage Message")
    print(""" Serial Summit protocol decoder will provide the conversation
    with summit systems timers over a direct serial link or 900MHz radio
    connected by serial or USBSerial. It will provide records at the end
    of the program being run, unless it is set up in either debug mode or 
    in MQTT mode.
    
    The MQTT mode is intended to provide the data directly on stdout for
    consumption by mosquitto_pub, but may be used in other ways too.
    
    The default mode of operation is to act on /dev/ttyUSB0, 9600 baud, 
    and produce data in tab-delimited format file on stdout when the program ends.
    
    Usage ( including defaults )
    protocol.py
        --help provides this message
        --dummy inserts some default data
        --debug
            Provide packet-level debug information as the process unfolds
            Value Options: '1','True','y','Y','true','TRUE' or no value are true
                --debug means True
                --debug=True means True
                --debug=0 means false
                --debug=Banana means false
        --devices=1,2,3,4,5,6,7,8,9
            A list of devices to poll in round-robin fashion. Must be a 
                comma-separated list of integers
        --port="/dev/ttyUSB0"  or --port="COM8"
            Must be a valid value for the operating system
            Valid Options in Linux: /dev/ttyUSB0, /dev/serial0, /dev/AMA1, etc
            Valid Options in Windows: Com1, Com2, Com3, Com4, ...ComN
            If not specified, will search for serial ports with "Digi" in the name.
        --output
            Output filename, with extension. 
            If file ends with .csv, then output will be in .CSV format, 
                otherwise tab-delimited text.
            Defaults to tab-delimited text format.
            Line endings will be OS-dependant.
            If no file identified, will print to the screen
        --mqtt  | --nomqtt
            Provide direct output during operation, providing JSON on stdout.
            On by default, with option to turn off
        --sync
            Send a sync signal over the serial port, which will properly 
            sync summits in range, but is not sync'd with computer clock
            time. There is a sync latency of between 0.25s and 0.30s to 
            computer clock time. There is no solution other than manual
            sync to a common GPS signal, or by calculating a correction
            factor after sync. This does not matter except to coordinate
            with devices sync'd to other clocks (like GPS). Consult the 
            summit timer documentation for procedures to perform a manual
            sync. Recommendations include using a GPS PPS signal gated to
            be once/minute.
        --api=[localhost:]8080 --noapi
            Operate webserver on port, optionally identifying hostname
            Turn webserver off with --noapi
        --api_format=[json,html]
            Determine data format for webserver. 
        --quiet
            Restricts sending of data on serial line. Usable for monitoring other software data streams
        --verbose
            When receiving data, will provide all recieved packets, rather than just data-carrying packets
        --download_only[=1]
            Download until all devices report exhaustion of records, with a timeout of 1 "AK" records

        Experimental Feature:
        --simulate_summits
            Write thread returns data to another controlling system.
            Acts as a simulated summit.
            Requires data to be loaded into it, possibly using API
            Use the "--devices" parameter to determine which devices will be simulated.

        API
        Retrieve records in a table / json 
        http://localhost:8080
        http://localhost:8080/{device}
        http://localhost:8080/{device}/{record}
        
        Retrive a specific attribute value from the record
        http://localhost:8080/{device}/{record}/{label}
        
        POST a new record
        curl --header "Content-Type: application/json" --request POST \
            --data {"device":9,"record":1,"event":0,"heat":0,"channel":1,"type":"p","label":null,"time":"01:02:03.321"} \
            http://localhost:8080
            
            "type" is one of "b":bib, "s":plunge, "y":sync and "p":post & Plunge
                Capital letters indicate the keyboard, rather than a plunger was used.
            "device", "type" are required.
            "label" is required when "type is [b,p]
            "record" will append a record if not specified, without filling holes in the sequence (max()+1).
            "event","heat" default 0
            "time" will take the current computer localtime if not specified
            "channel" will default to 1


    This software may also be extended by providing a live_output_callback 
    function in a subclass. Import both summit and main for normal behaviour.
    
    Sync over the serial port is caused by either putting the --sync flag
    on the command line (for startup sync) or by sending signal SIGUSR1. Sync is
    not handled by this software (as a simulated summit). Any change in the time 
    base is ignored. This is not the same behaviour as summit timers exhibit.

    """)



########################################################################
def main():
    kwargs=dict()
    args=list()
    for a in sys.argv[1:]:
        if '=' in a:
            k,v=a.strip('-').split('=')
            kwargs[k]=v
        else :
            args.append(a)
    
    if '--help' in args:
        usage()
        exit (0)

    with summit(*args,**kwargs) as s: 
        s.run()


if __name__ == '__main__':
    main()
